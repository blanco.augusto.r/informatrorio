"""
	Para convertir de un tipo de dato a otro se puede usar funciones.

	int() <<Convierte un dato en tipo entero

	str() <<Convierte un dato en tipo string

	float() <<Convierte un dato en tipo float
"""

numero = int(878.334)
print("\t",numero)
print("\t", type(numero))

print("-------------------------")

numero2 = float(423)
print("\t", numero2)
print("\t",type(numero2))

print("------------------------")

numero3 = str(464)
print("\t", numero3)
print("\t",type(numero3))