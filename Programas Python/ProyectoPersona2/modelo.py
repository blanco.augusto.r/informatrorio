from personaDB import DB

class Persona:

	def __init__(self,dni,nombre,edad,profesion,apellido=None,direccion=None):
		self.__dni = dni
		self.__nombre = nombre
		self.__edad = edad
		self.__profesion = profesion
		self.__apelido = apellido
		self.__direccion = direccion
		self.__DB = DB(name = 'Persona')
		self.__tableName = 'persona'

	def save(self):
		query = "INSERT INTO "+self.__tableName+"(dni,nombre,apellido,direccion,edad,profesion) VALUES(?,?,?,?,?,?)"
		values = (self.__dni,self.__nombre,self.__apelido,self.__direccion,self.__edad,self.__profesion)
		x = self.__DB.ejecutar(query,values)

	@classmethod
	def Todas(cls):
		query = "SELECT * FROM "+Persona.__tableName
		return Persona.__DB.ejecutar(query)