class Tiempo:
	def __init__(self,horas):
		self.horas = horas

	def getHora(self):
		return self.horas

	def setHora(self,nuevaHora):
		self.horas = nuevaHora

	# Creamos un propiedad con get y set de la hora
	hora = property(getHora, setHora)

# instanciamos el objeto y le damos valores
tiempo = Tiempo(1)


print(tiempo.hora)

# Metodo SET
# De esta forma es el metodo set, es la forma mas intuitiva para cambiar los valores
tiempo.hora = 2

# Metodo GET
#  De esta forma es el metodo get, es la forma mas instuitiva para llamar el valor 
tiempo.hora

print(tiempo.hora)


