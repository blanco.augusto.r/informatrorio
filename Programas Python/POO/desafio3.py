class Triangulos:
	def __init__(self,ladoA,ladoB,ladoC):
		self.ladoA = ladoA
		self.ladoB = ladoB
		self.ladoC = ladoC

	def tipoTriangulo(self):
		# Algoritmo de tipos de triangulos
		if self.ladoA==self.ladoB and self.ladoA==self.ladoC and self.ladoB==self.ladoC:
			return "El Triangulo es Equilatero"
		elif self.ladoA!=self.ladoB and self.ladoA!=self.ladoC and self.ladoB!=self.ladoC:
			return f" El Tiangulo es Escaleno y el lado maximo es {max([self.ladoA, self.ladoB, self.ladoC])}"
		else:
			return f' El Triagnulo es Isoceles y el lado maximo es {max([self.ladoA, self.ladoB, self.ladoC])}'

elEscalenox = Triangulos(2,6,4)
print(elEscalenox.tipoTriangulo())
print("---------------------------------------------------------")
elEquilaterox = Triangulos(2,2,2)
print(elEquilaterox.tipoTriangulo())
print("---------------------------------------------------------")
elIsoceles = Triangulos(3,3,6)
print(elIsoceles.tipoTriangulo())