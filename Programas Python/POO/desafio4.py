import os
# Definimos la clase contacto
class Contacto:
	# Cada dato se va a guardar en una lista
	def __init__(self, nombre=None, telefono=None, email=None):
		self.__nombre = nombre
		self.__telefono = telefono
		self.__email = email
	# Definimos los set y get
	def getNombre(self):
		return self.__nombre

	def getTelefono(self):
		return self.__telefono

	def getEmail(self):
		return self.__email

	def setNombre(self, agregarNom):
		self.__nombre = agregarNom

	def setTelefono(self, agregarTel):
		self.__telefono = agregarTel

	def setEmail(self, agregarEmail):
		self.__email = agregarEmail

# Definimos la clase agenda
class Agenda:
	def __init__(self):
		self.infoContactos = list()

	#Definimos el get para mostrar los datos
	def getInfoContactos(self):
		return self.infoContactos
	# Definimos el set para guardar cada contacto en un espacio en la lista
	def setInfoContactos(self, nombre, telefono, email):
		self.infoContactos.append([nombre, telefono, email])

# Instanciamos la clase contacto y agenda
contacto = Contacto()
agenda = Agenda()

def limpiarPantalla():
	os.system("cls")

def agregarContactos():
	print("------------- 1 - Añadir Contactos -------------")
	while True:
		nombreContacto = input("\n Nombre: ")
		telefonoContacto = int(input(" Numero: "))
		emailContacto = input(" Email: ")

		contacto.setNombre(nombreContacto)
		contacto.setTelefono(telefonoContacto)
		contacto.setEmail(emailContacto)

		agenda.setInfoContactos(contacto.getNombre(),contacto.getTelefono(),contacto.getEmail())

		salirAgregarContactos = input("\n¿Quiere añadir mas contactos? (Y/N)")
		salirAgregarContactos = salirAgregarContactos.upper()
		if salirAgregarContactos == 'N':
			break 

def listaContactos():
	while True:
		limpiarPantalla()
		print("------------- 2 - Lista Contactos -------------\n")
		cont = 0
		for i in agenda.infoContactos:
			cont+=1
			print(f" Contacto N°{cont}")
			print(f"\tNombre:{i[0]}")
			print(f"\tTelefono:{i[1]}")
			print(f"\tEmail:{i[2]}")


		salirListaContactos = input("\n¿Quiere volver a la lista de opciones? (Y/N)\n")
		salirListaContactos = salirListaContactos.upper()
		if salirListaContactos == 'Y':
			break 

def buscarContacto():
	while True:
		limpiarPantalla()
		print("------------- 3 - Buscar Contactos -------------\n")
		print(" Opciones:\n\t 1 - Buscar por Nombre.\n\t 2 - Buscar por Telefono.\n\t 3 - Buscar por Email")
		print("------------------------------------------------\n")		
		busqueda = int(input("Ingrese la opcion que desea elegir: "))

		if busqueda == 1:
			busquedaNombre = input("\nIngrese el nombre que desea buscar: ")
			for i in agenda.infoContactos:
				flag = busquedaNombre in i
				if flag:
					print(f"\n ¡El nombre {busquedaNombre} se ha encontrado!")
					break
				else:
					print(f"\n ¡El nombre {busquedaNombre} no se ha encontrado!")

		elif busqueda == 2:
			busquedaTelefono = int(input("\nIngrese el telefono que desea buscar: "))
			for i in agenda.infoContactos:
				flag = busquedaTelefono in i
				if flag:
					print(f"\n ¡El telefono {busquedaTelefono} se ha encontrado!")
				else:
					print(f"\n ¡El telefono {busquedaTelefono} no se ha encontrado!")
		elif busqueda == 3:
			busquedaEmail = input("\nIngrese el email que desea buscar: ")
			for i in agenda.infoContactos:
				flag = busquedaEmail in i
				if flag:
					print(f"\n ¡El email {busquedaEmail} se ha encontrado!")
				else:
					print(f"\n ¡El email {busquedaEmail} no se ha encontrado!")

		salirBusquedaContactos = input("\n¿Quiere volver a bucar otro contacto? (Y/N)\n")
		salirBusquedaContactos = salirBusquedaContactos.upper()
		if salirBusquedaContactos == 'N':
			break 		

def editarContacto():
	while True:
		limpiarPantalla()
		cont = 0
		for i in agenda.infoContactos:
			cont+=1
			print(f" Contacto N°{cont}")
			print(f"\tNombre:{i[0]}")
			print(f"\tTelefono:{i[1]}")
			print(f"\tEmail:{i[2]}")
			print("---------------------------")

		editarNroContacto = int(input(" Elija el numero de contacto que desea editar: "))

		print("\n------------- 4 - Editar Contactos -------------\n")
		print(" Opciones:\n\t 1 - Editar Nombre.\n\t 2 - Editar Telefono.\n\t 3 - Editar Email")
		print("------------------------------------------------\n")

		eleccion = int(input(" Ingrese la opcion que desea editar: "))

		if eleccion == 1:
			editarNombre = input("\t Editar Nombre: ")
			agenda.infoContactos[editarNroContacto][0] = editarNombre
		elif eleccion == 2:
			editarNombre = int(input("\t Editar Telefono: "))
			agenda.infoContactos[editarNroContacto][1]
		elif eleccion == 3:
			editarNombre = input("\t Editar Email: ")
			agenda.infoContactos[editarNroContacto][2]

		salirEditarContactos = input("\n¿Quiere volver a editar otro contacto? (Y/N)\n")
		salirEditarContactos = salirEditarContactos.upper()
		if salirEditarContactos == 'N':
			break 


# Creamos la interfaz con la que el usuario va a interactuar
def mostrarInterfaz():
	while True:

		limpiarPantalla()
		print("------------- Agenda de Contactos -------------")
		print(" Opciones:\n\t\t 1 - Añadir Contacto\n\t\t 2 - Lista de Contactos\n\t\t 3 - Buscar Contacto\n\t\t 4 - Editar Contacto\n\t\t 5 - Cerrar Agenda")
		print("-----------------------------------------------")

		while True:
			try:
				eleccion = int(input(" Ingrese la opcion que desea elegir: "))
				while eleccion!=1 and eleccion!=2 and eleccion!=3 and eleccion!=4 and eleccion!=5:
					eleccion = int(input(" Ingrese un valor correspondiente a las opciones: "))
				break
			except:
				print("ERROR: Datos incorrectos, ingrese un valor correspondiente a las opciones.")

		if eleccion == 1:
			limpiarPantalla()
			agregarContactos()
		elif eleccion == 2:
			limpiarPantalla()
			listaContactos()
		elif eleccion == 3:
			limpiarPantalla()
			buscarContacto()
		elif eleccion == 4:
			limpiarPantalla()
			editarContacto()
		elif eleccion == 5:
			break

mostrarInterfaz()