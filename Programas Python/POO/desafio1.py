class Vehiculo:
	def __init__(self, color, ruedas):
		self.color = color
		self.ruedas = ruedas

	def colorEs(self):
		print(f"El color del vehiculo es {self.color}")

	def ruedasEs(self):
		print(f"La cantidad de ruedas que tengo es {self.ruedas}")


class Coche(Vehiculo):
	def __init__(self, velocidad, cilindrada, color, ruedas):
		self.velocidad = velocidad
		self.cilindrada = cilindrada
		super().__init__(color, ruedas)

	def velocidadEs(self):
		print(f"La velocidad del vehiculo es km/h {self.velocidad}")

	def cilindradaEs(self):
		print(f"La cilindrada del vechiculo es {self.cilindrada}")

camion = Coche(1346, 150, "verde", 8)

camion.colorEs()
camion.ruedasEs()
camion.velocidadEs()
camion.cilindradaEs()