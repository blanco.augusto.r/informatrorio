class Instrumento:
	def __init__(self, precio):
		self.precio = precio

	def tocar(self):
		print("Estamos tocando musica")

	def precioDe(self):
		print(f"El precio de este instrumento es: ${self.precio}")

	def __str__(self):
		return f"Soy un instrumento"

class Bateria(Instrumento):
	pass

class Guitarra(Instrumento):
	def __init__(self, tipo, precio):
		self.tipo = tipo
		# Con la funcion super, se pueden obtener o heredar los metodos del clase padre
		super().__init__(precio)

	def getTipo(tipo):
		print(f"El tipo de guitarra es: {tipo}")

gibson = Guitarra("Electrica", 212451)

gibson.getTipo()
gibson.precioDe()
gibson.tocar()

# Cuando hacemos un print de un objeto, va a mostrar el str del objeto o de la clase padre si no se lo redefinio
print(gibson)