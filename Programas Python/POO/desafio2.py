class Vehiculo:
	def __init__(self, color, ruedas):
		self.color = color
		self.ruedas = ruedas

	def colorEs(self):
		print(f"El color del vehiculo es {self.color}")

	def ruedasEs(self):
		print(f"La cantidad de ruedas que tengo es {self.ruedas}")

class Coche(Vehiculo):
	def __init__(self, color, ruedas, velocidad, cilindrada):
		self.velocidad = velocidad
		self.cilindrada = cilindrada
		super().__init__(color, ruedas)

	def velocidadEs(self):
		print(f"La velocidad del vehiculo es km/h {self.velocidad}")

	def cilindradaEs(self):
		print(f"La cilindrada del vechiculo es {self.cilindrada}")

class Camioneta(Coche):
	def __init__(self, color, ruedas, velocidad, cilindrada, carga):
		self.carga = carga
		super().__init__(color, ruedas, velocidad, cilindrada)

	def cargaEs(self):
		print(f"La cantidad de kg que puede cargar es de {self.carga}kg")

class Bicicleta(Vehiculo):
	def __init__(self, color, ruedas, tipo):
		self.tipo = tipo
		super().__init__(color, ruedas)

	def tipoEs(self):
		print(f"El tipo de bicicleta es {self.tipo}")

class Motocicleta(Bicicleta):
	def __init__(self, color, ruedas, tipo, velocidad, cilindrada):
		self.velocidad = velocidad
		self.cilindrada = cilindrada
		super().__init__(color, ruedas, tipo)

	def tipoEs(self):
		print(f"El tipo de motocicleta es {self.tipo}")

	def velocidadEs(self):
		print(f"La velocidad del vehiculo es km/h {self.velocidad}")

	def cilindradaEs(self):
		return f"La cilindrada del vechiculo es {self.cilindrada}"

autoFordFiesta = Coche("verde", 4, 500, 450)
camionMerce = Camioneta("negro", 8, 300, 290, 1900)

BiciKawasaki = Bicicleta("rojo", 2, "urbana")
motoRouser = Motocicleta("celeste", 4, "deportivas", 1244, 190)

vehiculos = [autoFordFiesta, camionMerce, BiciKawasaki, motoRouser]

def catalogar(lista, cantRuedas):
	cantVconRuedas = 0
	for i in lista:
		if cantRuedas == i.ruedas:
			print(i)
			print(i.color)
			print(i.ruedas)
			if hasattr(i, "tipo") == True:
				print(i.tipo)
			if hasattr(i, "cilindrada") == True:
				print(i.cilindrada)
			if hasattr(i, "velocidad") == True:
				print(i.velocidad)
			if hasattr(i, "carga") == True:
				print(i.carga)
			print("----------------------------------")
			cantVconRuedas += 1
	print(f"Se han encontrado {cantVconRuedas} vehiculos con {cantRuedas}")



catalogar(vehiculos, 0)
catalogar(vehiculos, 2)
catalogar(vehiculos, 4)
catalogar(vehiculos, 8)