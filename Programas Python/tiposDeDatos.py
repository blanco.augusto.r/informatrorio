print("\n\tPrograma de tipos de datos\n")

enteros = 4
decimal = 535.234
booleanox = False
flotante = 4654.4654

# Tipo de dato None, sirve para inicializar una variable que no va a contener ningun tipo de dato, es decir que esta vacia y que se va a utilizar mas adelante
variableVacia = None

cadenaDeTexto = "La milanesa es lo mejor"
# Estas listas o arrays pueden cambiar su contenido
listaOArray = [87, 897, 837, 979]
listaConStrings = [12, "Petaco", "hermoso"]

# Las tuplas son array que no pueden cambiar su contenido
tupla = ("Augusto", "Blanco", "Rafael")

# Tambien existe listas tipo set, que son listas que ninguno de los elementos de su contenido no se repiten

# Los diccionarios guardan informacion de relación clave-valor
diccionario = {
	"nombre": "Augusto",
	"segundoNombre": "Rafael",
	"apellido": "Blanco"
}

# Tipo de dato rango, ejemplo, secuencia numerica del 0 al 9
rango = range(9)


print(tupla[2])