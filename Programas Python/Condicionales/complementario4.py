print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS CONDICIONALES - COMPLEMENTARIO 4 \n")
print("\t----------------------------------------------------\n")

print("\t Para saber si el número  es divisor que \"m\", ingrese los números acontinuación: \n")

n = int(input("\t Ingrese el número \"n\": "))
m = int(input("\t Ingrese el número \"m\": "))

if m%n == 0:
	print(f"\n\t El numero {n} es divisor de {m}.")
else:
	print(f"\n\t El numero {n} no es divisor de {m}.")