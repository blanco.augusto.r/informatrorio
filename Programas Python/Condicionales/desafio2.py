print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS CONDICIONALES - DESAFIO 2 \n")
print("\t----------------------------------------------------\n")

print("\t Segun el tamaño del pez, elija la opción correspondiente a los estandares: ")
print("\n\t 1 - Tamaño Normal.")
print("\n\t 2 - Tamaño por debajo de lo Normal.")
print("\n\t 3 - Tamaño un poco por encima de lo normal.")
print("\n\t 4 - Tamaño sobredimensionado.")

tamanioPez = int(input("\n\tIngrese el numero correspondiente a la opción: "))

if tamanioPez == 1:
	print("\n\t Pez en buenas condiciones.")
elif tamanioPez == 2:
	print("\n\t Pez con problemas de nutrición.")
elif tamanioPez == 3:
	print("\n\t Pez con síntomas de organismo contaminado.")
elif tamanioPez == 4:
	print("\n\t Pez contaminado.")
else:
	print("\n\t ERROR: Ingrese el valor correspondiente a las opciones.")