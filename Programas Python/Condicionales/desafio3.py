print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS CONDICIONALES - DESAFIO 3 \n")
print("\t----------------------------------------------------\n")

cantCompuesto = int(input("\n\t Ingrese la cantidad del porcentaje del compuesto que existe en su suelo: %"))

if cantCompuesto >= 10:
	matorral = input("\n\t Ingrese con la letra \"Y\" si existe matorral en su suelo, si no con la letra \"N\": ")
	if matorral == "N":
		print("\n\t Es factible la utilización de fertilizantes en su suelo.")
	else:
		print("\n\t No es factible la utilización de fertilizantes en su suelo.")
else:
	print("\n\t No es factible la utilización de fertilizantes en su suelo.")