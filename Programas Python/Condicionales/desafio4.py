print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS CONDICIONALES - DESAFIO 4 \n")
print("\t----------------------------------------------------\n")

print("\t----------------------")
print("\t    Lista de Recetas")
print("\t----------------------\n")

print("\t 1 - Ensalada de Lentapio")
print("\t 2 - Ensalada de Morrceb")

ingredientes1 = ["Lentejas", "Apio", "Verduras", "Berenjena"]
ingredientes2 = ["Morron", "Cebolla", "Verduras", "Berenjena"]

selecReceta = int(input("\t Ingrese el numero de la opcion que desea pedir: "))

if selecReceta == 1:

	print("\n\t Elija los tres ingredientes para la receta: ")
	print("\t 1 - Lentejas")
	print("\t 2 - Apio")
	print("\t 3 - Verduras")
	print("\t 4 - Berenjenas\n")
	eleccIng1 = int(input("\t Ingrese el primer ingrediente: "))
	eleccIng2 = int(input("\t Ingrese el segundo ingrediente: "))
	eleccIng3 = int(input("\t Ingrese el tercer ingrediente: "))

	print(f"\n\t Usted eligío la receta \"Lentapio\", con los ingredientes, {ingredientes1[eleccIng1-1]}, {ingredientes1[eleccIng2-1]}, {ingredientes1[eleccIng3-1]}")

elif selecReceta == 2:

	print("\n\t Elija los tres ingredientes para la receta: ")
	print("\t 1 - Morron")
	print("\t 2 - Cebolla")
	print("\t 3 - Verduras")
	print("\t 4 - Berenjenas\n")
	eleccIng1 = int(input("\t Ingrese el primer ingrediente: "))
	eleccIng2 = int(input("\t Ingrese el segundo ingrediente: "))
	eleccIng3 = int(input("\t Ingrese el tercer ingrediente: "))

	print(f"\n\t Usted eligío la receta \"Morrceb\", con los ingredientes, {ingredientes1[eleccIng1-1]}, {ingredientes1[eleccIng2-1]}, {ingredientes1[eleccIng3-1]}")

else:
	print("\n\t ERROR: El numero que ingreso no corresponde a ninguna de las opciones.")