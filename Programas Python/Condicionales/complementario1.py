print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS CONDICIONALES - COMPLEMENTARIO 1 \n")
print("\t----------------------------------------------------\n")

print("\n\t Ingrese tres números para ordenarlo de mayor a menor.\n")

num1 = int(input("Ingrese el primer número: "))
num2 = int(input("Ingrese el segundo número: "))
num3 = int(input("Ingrese el tercer número: "))

if num1>num2 and num1>num3:

	if num2>num3:
		print(f"\n\t >>>>{num1} {num2} {num3}<<<<")
	else:
		print(f"\n\t >>>>{num1} {num3} {num2}<<<<")

elif num2>num1 and num2>num3:

	if num1>num3:
		print(f"\n\t >>>>{num2} {num1} {num3}<<<<")
	else:
		print(f"\n\t >>>>{num2} {num3} {num1}<<<<")

elif num3>num1 and num3>num2:

	if num1>num2:
		print(f"\n\t >>>>{num3} {num1} {num2}<<<<")
	else:
		print(f"\n\t >>>>{num3} {num2} {num1}<<<<")
