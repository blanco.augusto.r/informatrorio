print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS CONDICIONALES - COMPLEMENTARIO 2 \n")
print("\t----------------------------------------------------\n")

numero = int(input("\n\t Ingrese un numero: "))

if numero%2 != 0:
	print(f"\n\t >> El numero {numero} es impar. <<")
else:
	print(f"\n\t >> El numero {numero} es par. <<")