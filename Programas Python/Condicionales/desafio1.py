print("----------------------------------------------------\n")
print("\t EJERCICIOS CONDICIONALES - DESAFIO 1 \n")
print("----------------------------------------------------\n")

cantAnhos = int(input("\tIngrese la cantidad de años: "))

if cantAnhos>=10:
	print("\n\tPor favor solicite revisión de suelos en su plantación.")
elif cantAnhos>0 and cantAnhos<10:
	print("\n\tIntentaremos ayudarte con un nuevo sistemas de control de plagas, y cuidaremos el suelo de tu plantación.")
else:
	print("\n\tError: Ingrese un valor superior a 0.")