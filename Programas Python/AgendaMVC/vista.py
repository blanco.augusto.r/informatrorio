class Vista:
	def __init__(self):
		# Opciones para usuarios
		self.op1Usuario = "Ingresar Usuario"
		self.op2Usuario = "Crear Usuario Nuevo"

		# Opciones para Contactos
		self.op1 = 'Añadir Contacto'
		self.op2 = 'Lista de Contactos'
		self.op3 = 'Buscar Contactos'
		self.op4 = 'Editar Contacto'
		self.op5 = 'Cerrar Agenda'

		# Opciones para busqueda y edición de contactos
		self.op1C = 'DNI'
		self.op2C = 'Nombre'
		self.op3C = 'Telefono'
		self.op4C = 'Email'


	def PedirDatos(self,*args):
		print("\n============== AGENDA - MVC ==============")
		print(" Ingresar Datos:")
		salida = dict()
		for r in args:
			salida[r] = input(r+": ")
		return salida

	def OpcionesUsuario(self):
		print("\n============== AGENDA - MVC ==============")
		print(" Opciones de Usuario:")
		print(f'\t1 - {self.op1Usuario}')
		print(f'\t2 - {self.op2Usuario}')
		print("==========================================")
		return int(input("  Elija una opcion >> "))

	def OpcionesContacto(self):
		print("\n============== AGENDA - MVC ==============")
		print(" Opciones de Contactos:")
		print(f'\t1 - {self.op1}')
		print(f'\t2 - {self.op2}')
		print(f'\t3 - {self.op3}')
		print(f'\t4 - {self.op4}')
		print(f'\t5 - {self.op5}')
		print("==========================================")
		return int(input("  Elija una opcion >> "))

	def AñadirContactos(self):
		print("\n============== AGENDA - MVC ==============")
		print(" Añadir Contactos:")

	def ListaContactos(self, contactos):
		i = 0
		print("\n============== AGENDA - MVC ==============")
		print(" Lista de Contactos:")
		for contacto in contactos:
			print(f'\n Contacto N°{i}:')
			print(f'\tDNI: {contacto[0]}')			
			print(f'\tNombre: {contacto[2]}')
			print(f'\tNumero: {contacto[3]}')
			print(f'\tEmail: {contacto[4]}')

			i=i+1

	def OpcionesBusqueda(self):
		print("\n============== AGENDA - MVC ==============")
		print(" Opciones de Busqueda de Contactos por:")
		print(f'\t1 - {self.op1C}')
		print(f'\t2 - {self.op2C}')
		print(f'\t3 - {self.op3C}')
		print(f'\t4 - {self.op4C}')
		return int(input("  Elija una opcion >> "))

	def OpcionesEditar(self):
		print("\n============== AGENDA - MVC ==============")
		print(" Opciones de Edición de Contactos por:")
		print(f'\t1 - Editar {self.op1C}')
		print(f'\t2 - Editar {self.op2C}')
		print(f'\t3 - Editar {self.op3C}')
		print(f'\t4 - Editar {self.op4C}')
		numOpcion = int(input("  Elija una opcion >> "))
		if numOpcion == 1:
			dato = int(input(f"  Ingrese el {self.op1C} que quiere reemplazar: "))
			dato2 = int(input(f"  Ingrese el dato {self.op1C} para reemplazar: "))
		elif numOpcion == 2:
			dato = input(f"  Ingrese el {self.op2C} que quiere reemplazar: ")
			dato2 = input(f"  Ingrese el  dato {self.op2C} para reemplazar: ")
		elif numOpcion == 3:
			dato = int(input(f"  Ingrese el {self.op3C} que quiere reemplazar: "))
			dato2 = int(input(f"  Ingrese el dato {self.op3C} para reemplazar: "))
		elif numOpcion == 4:
			dato = input(f"  Ingrese el {self.op2C} que quiere reemplazar: ")
			dato2 = input(f"  Ingrese el dato {self.op2C} para reemplazar: ")
			
		return numOpcion,dato,dato2






	# def BuscarContactos(self):
	# def EditarContacto(self):

