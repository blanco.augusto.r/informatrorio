from agendaDB import DB

class Usuario:
	__DB = DB(name = 'AgendaDB')
	__tableName = 'Agenda'

	def __init__(self,usuario,clave):
		self.__usuario = usuario
		self.__clave = clave

	def GuardarUsuario(self):
		query = "INSERT INTO "+Usuario.__tableName+"(usuario,clave) VALUES(?,?)"
		values = (self.__usuario, self.__clave)
		x = Usuario.__DB.ejecutar(query,values)

	def ValidarUsuario(self):
		query = "SELECT id_agenda,usuario, clave FROM "+Usuario.__tableName+" WHERE usuario='"+self.__usuario+"' AND clave='"+self.__clave+"'"
		x = self.__DB.ejecutar(query)
		if x:
			pk = x[0][0]
			return pk
		else:
			return 0

class Contacto(Usuario):
	__DB = DB(name = 'AgendaDB')
	__tableName = 'Contacto'

	def __init__(self,dni,id_agenda,nombre,telefono,email):
		self.__dni = dni
		self.__id_agenda = id_agenda
		self.__nombre = nombre
		self.__telefono = telefono
		self.__email = email


	def GuardarContacto(self):
		query = "INSERT INTO "+Contacto.__tableName+"(dni,id_agenda,nombre,telefono,email) VALUES(?,?,?,?,?)"
		values = (self.__dni,self.__id_agenda,self.__nombre,self.__telefono,self.__email)
		x = Contacto.__DB.ejecutar(query,values)

	@classmethod
	def QueryEditarContacto(cls,numOpcion,dato,dato2,opcionEditar):
		if numOpcion == 0 or numOpcion == 2:
			query = "UPDATE "+Contacto.__tableName+" SET "+opcionEditar+"="+dato2+" WHERE "+opcionEditar+"="+dato
		elif numOpcion == 1 or numOpcion == 3:
			query = "UPDATE "+Contacto.__tableName+" SET "+opcionEditar+"='"+dato2+"' WHERE "+opcionEditar+"='"+dato+"'"
		x = Contacto.__DB.ejecutar(query)
		return x


	@classmethod
	def QueryBuscarContacto(cls,opcion,numOpcion,busqueda):
		if numOpcion == 0 or numOpcion == 2:
			query = "SELECT dni,id_agenda,nombre,telefono,email FROM "+Contacto.__tableName+" WHERE "+opcion[numOpcion]+"="+busqueda
		elif numOpcion == 1 or numOpcion == 3:
			query = "SELECT dni,id_agenda,nombre,telefono,email FROM "+Contacto.__tableName+" WHERE "+opcion[numOpcion]+"='"+busqueda+"'"
		x = Contacto.__DB.ejecutar(query)
		return x

	@classmethod
	def Todas(cls):
		__DB = DB
		query = "SELECT * FROM "+Contacto.__tableName
		return Contacto.__DB.ejecutar(query)

