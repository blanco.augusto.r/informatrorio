from modelo_usuario import *
from modelo_contacto import *

from vista_usuario import *
from vista_contacto import *


class Controlador:
    def __init__(self):
        self.__vista = Vista()
        self.Inicio()

    def Inicio(self):
        # De esta forma inicializamos el menu
        opcionUsuario = self.__vista.OpcionesUsuario()

        if opcionUsuario == 1:
            pk = self.LoginUsuario()

            if pk > 0:
                print("\nSe ha logeado correctamente.\n")
                self.InicioContacto(pk)
            else:
                print("\nDatos Incorrectos, vuelva a ingresar sus datos.\n")
                self.Inicio()

        elif opcionUsuario == 2:
            self.AgregarUsuario()

    def InicioContacto(self, primaryKey):
        opcionContacto = self.__vista.OpcionesContacto()
        if opcionContacto == 1:
            #Añadir contactos
            self.AgregarContactos(primaryKey)
            self.InicioContacto(primaryKey)
        elif opcionContacto == 2:
            # Lista de Contactos
            self.ListaContactos()
            self.InicioContacto(primaryKey)
        elif opcionContacto == 3:
            self.BuscarContactos()
            self.InicioContacto(primaryKey)
        elif opcionContacto == 4:
            self.EditarContactos()
            self.InicioContacto(primaryKey)
        elif opcionContacto == 5:
            self.SalirAgenda()

    # LOGICA DE USUARIO

    def LoginUsuario(self):
        datos = self.__vista.PedirDatos('\tUsuario','\tContraseña')
        user = Usuario(datos['\tUsuario'],datos['\tContraseña'])
        pk = user.ValidarUsuario()
        return pk

    def AgregarUsuario(self):
        datos = self.__vista.PedirDatos('\tUsuario','\tContraseña')
        user = Usuario(datos['\tUsuario'],datos['\tContraseña'])
        user.GuardarUsuario()
        self.Inicio()

    # LOGICA DE CONTACTOS

    def AgregarContactos(self,fk):
        datos = self.__vista.PedirDatos('\tDNI','\tNombre','\tTelefono','\tEmail')
        Añadircontacto = Contacto(datos['\tDNI'],fk,datos['\tNombre'],datos['\tTelefono'],datos['\tEmail'],)
        Añadircontacto.GuardarContacto()


    def ListaContactos(self):
        contacto = Contacto.Todas()
        self.__vista.ListaContactos(contacto)

    def BuscarContactos(self):
        opciones = ('dni','nombre','telefono','email')
        opcionDeBusqueda = self.__vista.OpcionesBusqueda()
        opcionDeBusqueda = opcionDeBusqueda - 1
        busqueda = opciones[opcionDeBusqueda]
        datos = self.__vista.PedirDatos(busqueda)
        queryBusquedaContacto = Contacto.QueryBuscarContacto(opciones,opcionDeBusqueda,datos[busqueda])
        self.__vista.ListaContactos(queryBusquedaContacto)

    def EditarContactos(self):
        opciones = ('dni','nombre','telefono','email')
        opcionDeEditar = self.__vista.OpcionesEditar()
        numOpcion = opcionDeEditar[0] - 1
        dato = opcionDeEditar[1]
        dato2 = opcionDeEditar[2]
        eleccion = opciones[numOpcion]
        queryEditarContacto = Contacto.QueryEditarContacto(numOpcion,dato,dato2,eleccion)
        self.__vista.ListaContactos(queryEditarContacto)      

    
    def SalirAgenda(self):
        pass

Controlador()