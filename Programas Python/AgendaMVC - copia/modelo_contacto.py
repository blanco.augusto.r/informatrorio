from agendaDB import DB

class Contacto():
	__DB = DB(name = 'AgendaDB')
	__tableName = 'Contacto'

	def __init__(self,dni,id_agenda,nombre,telefono,email):
		self.__dni = dni
		self.__id_agenda = id_agenda
		self.__nombre = nombre
		self.__telefono = telefono
		self.__email = email


	def GuardarContacto(self):
		query = "INSERT INTO "+Contacto.__tableName+"(dni,id_agenda,nombre,telefono,email) VALUES(?,?,?,?,?)"
		values = (self.__dni,self.__id_agenda,self.__nombre,self.__telefono,self.__email)
		x = Contacto.__DB.ejecutar(query,values)

	@classmethod
	def QueryEditarContacto(numOpcion,dato,dato2,opcionEditar):
		if numOpcion == 0 or numOpcion == 2:
			query = "UPDATE "+Contacto.__tableName+" SET "+opcionEditar+"="+dato2+" WHERE "+opcionEditar+"="+dato
		elif numOpcion == 1 or numOpcion == 3:
			query = "UPDATE "+Contacto.__tableName+" SET "+opcionEditar+"='"+dato2+"' WHERE "+opcionEditar+"='"+dato+"'"
		x = Contacto.__DB.ejecutar(query)
		return x


	@classmethod
	def QueryBuscarContacto(opcion,numOpcion,busqueda):
		if numOpcion == 0 or numOpcion == 2:
			query = "SELECT dni,id_agenda,nombre,telefono,email FROM "+Contacto.__tableName+" WHERE "+opcion[numOpcion]+"="+busqueda
		elif numOpcion == 1 or numOpcion == 3:
			query = "SELECT dni,id_agenda,nombre,telefono,email FROM "+Contacto.__tableName+" WHERE "+opcion[numOpcion]+"='"+busqueda+"'"
		x = Contacto.__DB.ejecutar(query)
		return x

	@classmethod
	def Todas(cls):
		__DB = DB
		query = "SELECT * FROM "+Contacto.__tableName
		return Contacto.__DB.ejecutar(query)

