class Vista:
	def __init__(self):
		# Opciones para usuarios
		self.op1Usuario = "Ingresar Usuario"
		self.op2Usuario = "Crear Usuario Nuevo"

	def PedirDatos(self,*args):
		print("\n============== AGENDA - MVC ==============")
		print(" Ingresar Datos:")
		salida = dict()
		for r in args:
			salida[r] = input(r+": ")
		return salida

	def OpcionesUsuario(self):
		print("\n============== AGENDA - MVC ==============")
		print(" Opciones de Usuario:")
		print(f'\t1 - {self.op1Usuario}')
		print(f'\t2 - {self.op2Usuario}')
		print("==========================================")
		return int(input("  Elija una opcion >> "))

