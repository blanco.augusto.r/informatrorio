import pyodbc

def connect():
	servidor = 'AUGUSTO\SQLEXPRESS'
	nombre_bd = 'Persona'
	#nombre_usuario = ''
	#password = ''

	try:
		conexion = pyodbc.connect("DRIVER={SQL Server Native Client 11.0};"
								  "Server="+servidor+";"
								  "DATABASE="+nombre_bd+";"
								  "Trusted_Connection=yes;")

	except Exception as e:
		# Atrapar error
		print("Ocurrió un error al conectar a SQL Server: ", e)

	return conexion

def select(conn,query,values=None):
	c = conn.cursor()
	if values:
		c.execute(query,values)
	else:
		c.execute(query)
	for r in c:
		print(r)

def insert(conn,query,values):
	c = conn.cursor()
	c.execute(query,values)
	conn.commit()

def update(conn,query,values):
	c = conn.cursor()
	c.execute(query,values)
	conn.commit()

def delete(conn,query):
	c = conn.cursor()
	c.execute(query)
	conn.commit()


# ------------------------ PROGRAMA ------------------------
def menu():
	print("HOLA")
	while True:
		conn = connect()
		print("1--BUSCAR PERSONA\n2--CARGAR PERSONA\n3--MOSTRAR TODO\n4--ACTUALIZAR DATOS")
		op = input()

		if op == "1":
			dni = int(input("Ingrese el deni de quien quiere buscar: "))
			query = "SELECT * FROM persona WHERE dni = ?"
			values = (dni)
			select(conn,query,values)

		elif op=="2":
			print("Ingresee los datos de la persona")
			dni = int(input("DNI: "))
			nombre = input("Nombre: ")
			apellido = input("Apellido: ")
			direccion = input("Direccion: ")
			edad = int(input("Edad: "))
			profesion = input("Profesion: ")
			query = "INSERT INTO persona (dni,nombre,apellido,direccion,edad,profesion) VALUES(?,?,?,?,?,?)"
			values = (dni,nombre,apellido,direccion,edad,profesion)
			insert(conn,query,values)

		elif op=="3":
			query = "SELECT * FROM persona"
			select(conn, query)

		elif op == "4":
			dni = int(input("Ingrese el dni de la persona que desea editar: "))
			columna = input("Ingrese el nombre de la columna que desea editar: ")
			edit = input("Ingrese el nombre por lo que quiere editar: ")
			query = f"UPDATE Persona SET {columna} = ? WHERE dni = ?"
			values = (edit,dni)
			update(conn,query,values)
		else:
			print("ADIOS")
			break
		print("###########################################")
		conn.close()

menu()
# consulta = "INSERT INTO persona (dni,nombre,apellido,direccion,edad,profesion) VALUES(?,?,?,?,?,?)"
# v = (12345672,"Gonzalitox","Fuccio","Avenida Siempre Viva",26,"Albañil")

# insert(conn,consulta,v)
# consulta = "SELECT * FROM persona"
# select(conn,consulta)


# conn.close()
# query = "SELECT * FROM persona"

# select(conn,query)

# conn = connect()

# query = "INSERT INTO persona 
# valores = (42262636,"Juan","Perez","Barrio San Cayetano",22,"Arquitecto")

# conn.cursor()


# EXECUTE REALIZA LA CONSULTA EN SQL
# conn.execute(query,valores)
# conn.commit()
# conn.close()

