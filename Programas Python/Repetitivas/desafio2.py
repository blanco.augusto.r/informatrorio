print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS REPETITIVAS - DESAFIO 2 \n")
print("\t----------------------------------------------------\n")

cantPersonas = int(input("\t Ingrese la cantidad de personas que recolectaron colillas: "))
print("\n")

cantMenosCien = 0
cantEntreCyD = 0
cantMasDos = 0

for i in range(1,cantPersonas+1):

	coll_por_personas = int(input(f"\t Persona {i}: Ingrese la cantidad que recolecto: "))

	if coll_por_personas < 100:
		#Menos de 100
		cantMenosCien += 1

	elif coll_por_personas < 200:
		#Menos de 200
		cantEntreCyD += 1
	elif coll_por_personas > 200:
		#Mas de 200
		cantMasDos += 1

porcent1 = (cantMenosCien*100)/cantPersonas
porcent2 = (cantEntreCyD*100)/cantPersonas
porcent3 = (cantMasDos*100)/cantPersonas

print(f"\t El porcentaje de personas que encontraron menos de 100 colillas es: {int(porcent1)}%.")
print(f"\t El porcentaje de personas que encontraron mas de 100 colillas y menos de 200 es: {int(porcent2)}%.")
print(f"\t El porcentaje de personas que encontraron mas de 200 colillas es: {int(porcent3)}%.")