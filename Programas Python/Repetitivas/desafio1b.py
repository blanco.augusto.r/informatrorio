print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS REPETITIVAS - DESAFIO 1.b \n")
print("\t----------------------------------------------------\n")

usuario = "puchaQueSoyBueno2"
contrasenia = "quisquillosos2020"

print(f"\t Usuario: {usuario}")
entrada_contrasenia = input("\tIngrese su contraseña: ")

contador = 1

while contrasenia != entrada_contrasenia and contador <= 3:
	entrada_contrasenia = input(f"\n\tIntento {contador}: ERROR: Contraseña incorrecta, ingrese nuevamente su contraseña: ")
	contador += 1
	

if contrasenia == entrada_contrasenia:
	print("\n\t ¡Usted a ingresado a su cuenta!")


if contador >= 3:
	print("\n\t CUENTA BLOQUEADA: Tres intentos fallidos. Se cerrara su cuenta y se le sacara todo su dinero >:)")