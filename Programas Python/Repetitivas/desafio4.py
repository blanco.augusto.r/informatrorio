import os
os.system('color')



print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS REPETITIVAS - DESAFIO 4 \n")
print("\t----------------------------------------------------\n")


print("\t Ingrese el tamaño del tablero ecológico a imprimir: \n")

ancho = int(input("Ingrese el ancho del tablero: "))
alto = int(input("Ingrese el alto del tablero: "))

print("\n")

for i in range(alto):
	filas = i+1

	for x in range(ancho):
		columnas = x+1

		if filas%2 != 0:
			if columnas%2 != 0:
				print("\033[1;33;40m[x]",end="")
			else:
				print("\033[1;32;40m[o]",end="")
		else:
			if columnas%2 != 0:
				print("\033[1;32;40m[o]",end="")
			else:
				print("\033[1;33;40m[x]",end="")
	print("")
print("\n")