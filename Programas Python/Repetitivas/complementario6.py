print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS REPETITIVAS - COMPLEMENTARIO 6 \n")
print("\t----------------------------------------------------\n")

print("\t ------------- NUMEROS PRIMOS -------------\n")
numeroIngres = int(input("\t Ingrese el numero a evaluar: "))

cont = 1
contPrimos = 0

while cont <= numeroIngres:

	if numeroIngres%cont == 0:
		contPrimos += 1

	cont += 1

if contPrimos == 2:
	print(f"\t El numero {numeroIngres} es un numero primo.")
else:
	print(f"\t El numero {numeroIngres} no es un numero primo.")


#Otra forma de ver cuando es primo o no

N = int(input("Ingrese el numero: \t"))
primo = True
c=2
while primo and (c < N):
	if N % c == 0:
		primo = False
	c+=1

if primo:
	print("El numero es primo.")
else:
	print("El numero no es primo.")