print("\t----------------------------------------------------\n")
print("\t\t EJERCICIOS REPETITIVAS - DESAFIO 1.a \n")
print("\t----------------------------------------------------\n")

usuario = "puchaQueSoyBueno2"
contrasenia = "quisquillosos2020"

print(f"\t Usuario: {usuario}")
entrada_contrasenia = input("\tIngrese su contraseña: ")

while contrasenia != entrada_contrasenia:
	entrada_contrasenia = input("\n\tERROR: Contraseña incorrecta, ingrese nuevamente su contraseña: ")

if contrasenia == entrada_contrasenia:
	print("\n\t ¡Usted a ingresado a su cuenta!")