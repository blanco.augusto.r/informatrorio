import pyodbc


class DB:

	def __init__(self,name,server='AUGUSTO\SQLEXPRESS',driver="SQL Server Native Client 11.0"):
		self.__name = name
		self.__server = server
		self.__driver = driver
		self.__conexion = None
		self.__dato = None


	def conectar(self):
		#CREO LA CONEXION
		self.__conexion = pyodbc.connect("DRIVER={"+self.__driver+"};"
										"Server="+self.__server+";"
										"DATABASE="+self.__name+";"
										"Trusted_Connection=yes;")

	def crusor(self):
		self.__cursor = self.__conexion.cursor()
	def commit(self,query):
		#  Enviar commit
		esselect = query.count('SELECT')
		if esselect == 0:
			self.__conexion.commit()
	def cerrar(self):
		self.__conexion.close()
	def obtener_datos(self,query):
		esselect = query.count('SELECT')
		if esselect > 0:
			self.__datos = self.__cursor.fetchall()

	def consulta(self,query,values=None):
		if values:
			self.__cursor.execute(query,values)
		else:
			self.__cursor.execute(query)

	def ejecutar(self,query,values=None):
		self.conectar()
		self.crusor()
		self.consulta(query,values)
		self.commit(query)
		self.obtener_datos(query)
		self.cerrar()

		return self.__dato
