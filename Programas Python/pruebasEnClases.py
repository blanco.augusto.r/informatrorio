suma = 5+6
resta = 6-8
division = 535/65
multiplicacion = 324*23
potenciacion = 2*2
complejos = 5.6 + 7j
boleanos = True
listas = [0.45, "Manzana", 3412]
conjunto = {4314, "Apple", True}
tupla = 1, 53, "Perfecto"
elementoVacio = None
diccionario = {
	"nombre": "Augusto",
	"segundoNombre": "Rafael",
	"apellido": "Blanco"
}

print("---------------- DATE TYPES -----------------")
print("\t Suma: ", type(suma))
print("\t Resta: ", type(resta))
print("\t Division: ", type(division))
print("\t Multiplicacion: ", type(multiplicacion))
print("\t Potenciacion: ", type(potenciacion))
print("\t Complejos: ", type(complejos))
print("\t Boleanos: ", type(boleanos))
print("\t Listas: ", type(listas))
print("\t Conjuntos: ", type(conjunto))
print("\t Tuplas: ", type(tupla))
print("\t Variable Vacia: ", type(elementoVacio))
print("\t Diccionarios: ", type(diccionario))